# Disciplina : Arquitetura e Organizacao de Computadores
# Atividade : Avaliacao 01 - Programacao em Linguagem de Montagem
# Programa 02
# Grupo : - Edgar Luis Brissow
#	  - Fernanda Lucia Santana


.data 	#Segmento de Dados
	Msg1:   	.asciiz "\n\n Entre com o tamanho do vetor (max. =8 : "
	Msg2:		.asciiz "\n Valor Invalido"
	Msg3:		.asciiz "\n Entre com um numero (max. = 8): "
	Msg4_1: 	.asciiz "\n Vetor A["
	Msg5_1: 	.asciiz "\n Vetor B["
	Msg6_1:		.asciiz "\n Vetor C["
	Msg4_2:		.asciiz "] : "
	
	# defincao do Vetor_A[]. Iniciando o Vetor_1[] = 0 na memoria
	Vetor_A: 	.word 0, 0, 0, 0, 0, 0, 0, 0
	Vetor_B: 	.word 0, 0, 0, 0, 0, 0, 0, 0
	Vetor_C: 	.word 0, 0, 0, 0, 0, 0, 0, 0
.text	#Segmento de codigo
	main:
			la 	$s5, Vetor_A		#$s5 <- &A[0]
			la 	$s6, Vetor_B		#$s6 <- &B[0]
			la 	$s7, Vetor_C		#$s7 <- &C[0]		
			
			add 	$s3, $zero, 0		#$ s3 <- num = 0
			add 	$t0,$0,$0		# $t0 <- i = 0
			add	$s2, $0, $0		# $s2 <- 0
			
	Validacao:					# Inicio do Laco do{
			 # PRINT_STRING
      			li  	$v0, 4                	# chamada 4
      			la  	$a0, Msg1             	# Msg1
      			syscall
      			
      			 # READ_INT
      			li  	$v0, 5               	# chamada 5 
      			syscall                    
      			add 	$s3, $v0, $0         	# salva $v0 em $s3	
      			
      			slti	$t5,$s3,2		# Verifica se num < 2
      			bne	$t5,$zero,Error		# se num < 2 vai pra msg de Erro
      			slti	$t5,$s3,9		# se meu num < 9
      			beq	$t5,$zero,Error		# se num >=9 vai pra msg de Erro
      			addi 	$t0,$zero, 0		# $t0 = 0
      			j 	VetorA			# Vai para o lace que adiciona numeros no Vetor A[]
      			
      			
      	Error:		 # Mensagem caso num < 2 && num >8	
      			li  $v0, 4                	# chamada 4
      			la  $a0, Msg2             	# Msg2
      			syscall                    
			j	Validacao		#continua no laco }while( num < 2 && num >8);
	

	VetorA:	        #PRINT_STRING
			li $v0, 4 			# chamada para escrever string
			la $a0, Msg4_1			# Msg4_1
			syscall
			
			li 	$v0,1			# chamda 1 para escrever Inteiro posicoes do vetor
			add 	$a0,$0, $t0		# adiciona $a0 <- 0
			syscall
			
			li 	$v0, 4 			# chamada para escrever string
			la 	$a0, Msg4_2		# Msg4_2
			syscall
			
			li 	$v0 , 5			# chamada5
			syscall
			
			add 	$s0, $v0, $0		# s0 <- v0			
			add 	$s2, $t0, $t0		# s2 <- i*2
			add 	$s2, $s2,$s2		# s2*4
			add 	$s2,$s2,$s5		# s2 <- Vetor_A[i]
			sw  	$s0, 0($s2)		# Adiciona na memoria Vetor_A[i] <- $s0
			addi 	$t0, $t0, 1		# i++
			
			bne $t0,$s3, VetorA 		# for (i=0 ; i!=num; i++)
			
			add $t0, $0,$0			# i = 0
			
			
	VetorB:		#PRINT_STRING
			li 	$v0, 4 			#chamada para escrever string
			la 	$a0, Msg5_1		#Msg5_1
			syscall
			
			li 	$v0,1			# chamda 1 para escrever Inteiro posicoes do vetor
			add 	$a0,$0, $t0		#$a0 <- 0
			syscall
			
			li 	$v0, 4 			#chamada para escrever string
			la 	$a0, Msg4_2		#Msg4_2
			syscall
			
			li 	$v0 , 5			#chamada5
			syscall
			
			add 	$s0, $v0, $0		# s1 <- v0
			add 	$s2, $t0, $t0		#s2 <- i*2
			add 	$s2, $s2,$s2		#s2*4
			add 	$s2,$s2,$s6		#s2 <- Vetor_B[i]
			sw  	$s0, 0($s2)		# Vetor_B[i] <- s0
			addi 	$t0, $t0, 1		# i++
			
			bne	$t0,$s3, VetorB		# for(i = 0; i!=num; i++)
			
			add $t0, $0,$0			# i=0
			
	Sum:		
			add 	$s2, $t0, $t0		#s2 <- i.2
			add 	$s2, $s2,$s2		#s2.4
			add 	$s2,$s2,$s5		#$s2 <- Vetor_A[i]
			lw  	$t1, 0($s2)		#t1 <- Vetor_A[i]
      			add 	$s2, $t0, $t0		#s2 <- i.2
			add 	$s2, $s2,$s2		#s2.4      			
      			add 	$s2, $s2, $s6		#$s2 <- Vetor_B[i]
      			lw  	$t2, 0($s2)		#t2 <- Vetor_B[i]
      			add 	$s2, $t0, $t0		#s2 <- i.2
			add 	$s2, $s2,$s2		#s2.4
      			add 	$s2, $s2, $s7		#$s2 <- Vetor_C[i]
      			add 	$s4, $t1,$t2		#$s4 <- A + B
      			sw 	$s4, 0($s2)		# Adiciona na memoria Vetor_A[i] <- $s0
      			addi 	$t0, $t0, 1		# i++
      			
      			bne 	$t0, $s3, Sum		#se diferente de num retorna para o laco
      			add 	$t0,$0,$0		#$t0<-0
      	Show_sum: 
      			li 	$v0, 4 			#chamada para escrever string
			la 	$a0, Msg6_1		#Msg6_1
			syscall
			
			li 	$v0,1			#chamada variavel inteira
			add 	$a0,$0, $t0		# $a0<-0
			syscall
			
			li 	$v0, 4 			#chamada para escrever string
			la 	$a0, Msg4_2		#Msg4_2
			syscall
			
			add 	$s2, $t0, $t0		#s2 <- i.2
			add 	$s2, $s2,$s2		#s2.4
      			add 	$s2, $s2, $s7		#$s2 <- Vetor_C[i]
      			lw 	$t1, 0($s2)		# Vetor_B[i] <- t1
      			addi 	$t0, $t0, 1		#i++
      			li 	$v0,1			#Chamada le int
			add 	$a0,$0, $t1		#a0 <- t1
			syscall
			
			bne $t0, $s3, Show_sum		#for(i=0, i!=num;i++)
